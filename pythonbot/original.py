#---------------------------------------------------------------------#
# Warlight AI Challenge - Starter Bot                                 #
# ============                                                        #
#                                                                     #
# Last update: 20 Mar, 2014                                           #
#                                                                     #
# @author Jackie <jackie@starapple.nl>                                #
# @version 1.0                                                        #
# @license MIT License (http://opensource.org/licenses/MIT)           # 
#---------------------------------------------------------------------#

from math import fmod, pi
from sys import stderr, stdin, stdout
from time import clock
import random
from datetime import datetime

class Bot(object):
    '''
    Main bot class
    '''
    def __init__(self):
        '''
        Initializes a map instance and an empty dict for settings
        '''
        self.settings = {}
        self.map = Map()

    def run(self):
        '''
        Main loop
        
        Keeps running while being fed data from stdin.
        Writes output to stdout, remember to flush!
        '''
        while not stdin.closed:
            try:
                rawline = stdin.readline()

                # End of file check
                if len(rawline) == 0:
                    break

                line = rawline.strip()

                # Empty lines can be ignored
                if len(line) == 0:
                    continue

                parts = line.split()

                command = parts[0]

                # All different commands besides the opponents' moves
                if command == 'settings':
                    self.update_settings(parts[1:])

                elif command == 'setup_map':
                    self.setup_map(parts[1:])

                elif command == 'update_map':
                    self.update_map(parts[1:])

                elif command == 'pick_starting_regions':
                    stdout.write(self.pick_starting_regions(parts[2:]) + '\n')
                    stdout.flush()

                elif command == 'opponent_moves':
                    stdout.flush()

                elif command == 'go':

                    sub_command = parts[1]

                    if sub_command == 'place_armies':

                        stdout.write(self.place_troops() + '\n')
                        stdout.flush()

                    elif sub_command == 'attack/transfer':

                        stdout.write(self.attack_transfer() + '\n')
                        stdout.flush()

                    else:
                        stderr.write('Unknown sub command: %s\n' % (sub_command))
                        stderr.flush()

                else:
                    stderr.write('Unknown command: %s\n' % (command))
                    stderr.flush()
            except EOFError:
                return
    
    def update_settings(self, options):
        '''
        Method to update game settings at the start of a new game.
        '''
        key, value = options
        self.settings[key] = value

    def setup_map(self, options):
        '''
        Method to set up essential map data given by the server.
        '''
        map_type = options[0]

        for i in range(1, len(options), 2):

            if map_type == 'super_regions':

                super_region = SuperRegion(options[i], int(options[i + 1]))
                self.map.super_regions.append(super_region)

            elif map_type == 'regions':

                super_region = self.map.get_super_region_by_id(options[i + 1])
                region = Region(options[i], super_region)
                
                self.map.regions.append(region)
                super_region.regions.append(region)

            elif map_type == 'neighbors':

                region = self.map.get_region_by_id(options[i])
                neighbours = [self.map.get_region_by_id(region_id) for region_id in options[i + 1].split(',')]

                for neighbour in neighbours:
                    region.neighbours.append(neighbour)
                    neighbour.neighbours.append(region)

        if map_type == 'neighbors':
            
            for region in self.map.regions:

                if region.is_on_super_region_border:
                    continue

                for neighbour in region.neighbours:

                    if neighbour.super_region.id != region.super_region.id:

                        region.is_on_super_region_border = True
                        neighbour.is_on_super_region_border = True

    def update_map(self, options):
        '''
        Method to update our map every round.
        '''
        for i in range(0, len(options), 3):
            
            region = self.map.get_region_by_id(options[i])
            region.owner = options[i + 1]
            region.troop_count = int(options[i + 2])
            
    def pick_starting_regions(self, options):
        '''
        Method to select our initial starting regions.
        
        Currently selects six random regions.
        '''
        #shuffled_regions = Random.shuffle(Random.shuffle(options))
        #return ' '.join(shuffled_regions[:6])
        shuffled_regions = Random.shuffle(options)
        random.shuffle(options)
        SA_NA_AUS = ["1","2","3","4","5","6","7","8","9","10","11","12","13","39","40","41","42"]
        chosen_regions = []
        for region in shuffled_regions:
            if region in SA_NA_AUS:
                chosen_regions.append(region)
        return ' '.join(chosen_regions)

    def place_troops(self):
        '''
        Method to place our troops.
        
        Currently keeps placing a maximum of two troops on random regions.
        '''
        placements = []
        place_size = 2
        region_index = 0
        troops_remaining = int(self.settings['starting_armies'])
        
        owned_regions = self.map.get_owned_regions(self.settings['your_bot'])
        
        # states:   number enemies on border
        #           relative strength of enemies
        #           relative strength of me
        #           on super region border
        #           unowned regions in my super region
        #duplicated_regions = owned_regions * (3 + int(troops_remaining / 2))
        #shuffled_regions = Random.shuffle(duplicated_regions)

        # this is pretty good, but struggles at endgame
        p0 = [r for r in owned_regions if r.hasEnemyNeighbours() and r.is_on_super_region_border]
        p1 = [r for r in owned_regions if r.hasEnemyNeighbours()]
        p3 = [r for r in owned_regions if r.is_on_super_region_border]
        p2 = [r for r in owned_regions if r.hasNonMeNeighbours()]
        shuffled_regions = Random.shuffle(p0+p0+p0+p0+p1+p1+p1+p2+p2+p3+p3+owned_regions)
        while troops_remaining:

            region = shuffled_regions[region_index]
            
            if troops_remaining > place_size-1:
                placements.append([region.id, place_size])
                region.troop_count += place_size
                troops_remaining -= place_size
                
            else:
                 placements.append([region.id, 1])
                 region.troop_count += 1
                 troops_remaining -= 1

            region_index += 1
        


        return ', '.join(['%s place_armies %s %d' % (self.settings['your_bot'], placement[0],
            placement[1]) for placement in placements])


    def attack_transfer(self):
        '''
        Method to attack another region or transfer troops to allied regions.
        
        Currently checks whether a region has more than six troops placed to attack,
        or transfers if more than 1 unit is available.
        '''
        def attackable(fromR, toR):
            return fromR.troop_count > toR.troop_count + 1

        def moveable(fromR):
            return fromR.troop_count > 1

        def share_enemies(ra,rb):
            for a in ra.getEnemyNeighbours():
                if a in rb.getEnemyNeighbours():
                    return True
            return False

        def get_attack_armies(region):
            return region.troop_count - 1

        def get_move_armies(region):
            return region.troop_count - 1

        def get_region_to_move_to(region, lookahead):
            if lookahead == 0:
                return None
            neutral_neighbours = region.getNeutralNeighbours()
            enemy_neighbours = region.getEnemyNeighbours()
            allied_neighbours = region.getAlliedNeighbours()
            attackable_enemy_neighbours = [r for r in enemy_neighbours if attackable(region,r)]
            attackable_enemy_neighbours_inSR = [r for r in attackable_enemy_neighbours if attackable(region,r) and r.super_region.id is region.super_region.id]
            attackable_neutral_neighbours = [r for r in neutral_neighbours if attackable(region,r)]
            attackable_neutral_neighbours_inSR = [r for r in attackable_neutral_neighbours if attackable(region,r) and r.super_region.id is region.super_region.id]
            allied_neighbours_with_shared_enemy = [r for r in allied_neighbours if share_enemies(r, region)]
            has_enemy_neighbours = len(enemy_neighbours) > 0
            has_neutral_neighbours = len(neutral_neighbours) > 0
            has_allied_neighbours_with_shared_enemy = len(allied_neighbours_with_shared_enemy) > 0
            has_attackable_enemy_neighbours = len(attackable_enemy_neighbours) > 0
            has_attackable_enemy_neighbours_inSR = len(attackable_enemy_neighbours_inSR)
            has_attackable_neutral_neighbours = len(attackable_neutral_neighbours) > 0
            has_attackable_neutral_neighbours_inSR = len(attackable_neutral_neighbours_inSR) > 0

            #todo add some randomization bw similar priorities
            if has_attackable_enemy_neighbours_inSR:  #p1
                #attack one of them
                return attackable_enemy_neighbours_inSR[0]
            elif has_attackable_enemy_neighbours:  #p2
                #attack one of them
                return attackable_enemy_neighbours[0]
            elif has_allied_neighbours_with_shared_enemy:  #p3
                # move to one of them
                return allied_neighbours_with_shared_enemy[0]
            elif has_attackable_neutral_neighbours_inSR:  #p3
                # attack one of them
                return attackable_neutral_neighbours_inSR[0]
            elif has_attackable_neutral_neighbours:
                # attack one of them
                return attackable_neutral_neighbours[0]

            elif not has_neutral_neighbours and not has_enemy_neighbours and len(allied_neighbours)>0:
                # move to get_region_to_move_to(one of my neighbours, maybe add randomizer as to which to choose)
                return get_region_to_move_to(allied_neighbours[0], lookahead-1)
            else:
                None
        # also transfer to a region if
        # it has a neighbor that also borders same enemy as me, and i dont have enough to attack

        attack_transfers = []
        owned_regions = self.map.get_owned_regions(self.settings['your_bot'])
        usable_regions = [r for r in owned_regions if r.troop_count > 1]
        for region in usable_regions:
            # TODO get bool is atk or tsf, have better decision for armies
            to_region = get_region_to_move_to(region,2)
            armies = get_attack_armies(region)
            if to_region:
                attack_transfers.append([region.id, to_region.id, armies])
        """
        for region in owned_regions:
            if region.troop_count <= 1:
                pass
            else:
                bad_ns = region.getNonMeNeighbours()
                if len(bad_ns) == 0:
                    # move to neighbor either on, border with enemy, srborder, less neighbors
                    borders_enemy_ns = [r for r in region.neighbours if r.hasEnemyNeighbours()]
                    borders_nonme_ns = [r for r in region.neighbours if r.hasNonMeNeighbours()]
                    borders_sr_ns = [r for r in region.neighbours if r.is_on_super_region_border]
                    if len(borders_enemy_ns) > 0:
                        attack_transfers.append([region.id, borders_enemy_ns[0].id, region.troop_count-1])
                    elif len(borders_nonme_ns) > 0:
                        attack_transfers.append([region.id, borders_nonme_ns[0].id, region.troop_count-1])
                    elif len(borders_sr_ns) > 0:
                        attack_transfers.append([region.id, borders_sr_ns[0].id, region.troop_count-1])
                    else:
                        attack_transfers.append([region.id, region.neighbours[0].id, region.troop_count-1])
                else:
                    enemy_sr_ns = region.getEnemyNeighboursInSuper()
                    nonme_sr_ns = region.getNonMeNeighboursInSuper()
                    to_atk = [r for r in enemy_sr_ns if attackable(region.troop_count, r.troop_count)==True ]
                    if len(to_atk) > 0:
                        attack_transfers.append([region.id, to_atk[0].id, region.troop_count-1])
                    else:
                        to_atk = [r for r in nonme_sr_ns if attackable(region.troop_count, r.troop_count)]
                        if len(to_atk) > 0:
                            attack_transfers.append([region.id, to_atk[0].id, region.troop_count-1])
                        else:
                            to_atk = [r for r in bad_ns if attackable(region.troop_count, r.troop_count) ]
                            if len(to_atk) > 0:
                                attack_transfers.append([region.id, to_atk[0].id, region.troop_count-1])
        """

        if len(attack_transfers) == 0:
            return 'No moves'
        
        return ', '.join(['%s attack/transfer %s %s %s' % (self.settings['your_bot'], attack_transfer[0],
            attack_transfer[1], attack_transfer[2]) for attack_transfer in attack_transfers])


class Map(object):
    '''
    Map class
    '''
    def __init__(self):
        '''
        Initializes empty lists for regions and super regions.
        '''
        self.regions = []
        self.super_regions = []

    def get_region_by_id(self, region_id):
        '''
        Returns a region instance by id.
        '''
        return [region for region in self.regions if region.id == region_id][0]
    
    def get_super_region_by_id(self, super_region_id):
        '''
        Returns a super region instance by id.
        '''
        return [super_region for super_region in self.super_regions if super_region.id == super_region_id][0]

    def get_owned_regions(self, owner):
        '''
        Returns a list of region instances owned by `owner`.
        '''
        return [region for region in self.regions if region.owner == owner]

class SuperRegion(object):
    '''
    Super Region class
    '''
    def __init__(self, super_region_id, worth):
        '''
        Initializes with an id, the super region's worth and an empty lists for 
        regions located inside this super region
        '''
        self.id = super_region_id
        self.worth = worth
        self.regions = []

class Region(object):
    '''
    Region class
    '''
    def __init__(self, region_id, super_region):
        '''
        '''
        self.id = region_id
        self.owner = 'neutral'
        self.neighbours = []
        self.troop_count = 2
        self.super_region = super_region
        self.is_on_super_region_border = False

    def getNumNeighbours(self):
        return len(self.neighbours)

    def getEnemyNeighbours(self):
        return [r for r in self.neighbours if r.owner != self.owner and r.owner != 'neutral']
    
    def getAlliedNeighbours(self):
        return [r for r in self.neighbours if r.owner == self.owner]

    def getNonMeNeighbours(self):
        return [r for r in self.neighbours if r.owner != self.owner]

    def getEnemyNeighboursInSuper(self):
        return [r for r in self.neighbours if r.owner != self.owner and r.super_region.id == self.super_region.id and r.owner != 'neutral']

    def getNonMeNeighboursInSuper(self):
        return [r for r in self.neighbours if r.owner != self.owner and r.super_region.id == self.super_region.id and r.owner != 'neutral']

    def getNeutralNeighbours(self):
        return [r for r in self.neighbours if r.owner is 'neutral']

    def hasNonMeNeighbours(self):
        return len(self.getNonMeNeighbours()) > 0

    def hasEnemyNeighbours(self):
        return len(self.getEnemyNeighbours()) > 0

    def getStrongestEnemyNeighbour(self):
        mx = 0
        strongest = None
        for r in self.neighbours:
            if mx < r.troop_count:
                mx = r.troop_count
                strongest = r
        return r

class Random(object):
    '''
    Random class
    '''
    @staticmethod
    def randrange(min, max):
        '''
        A pseudo random number generator to replace random.randrange
        
        Works with an inclusive left bound and exclusive right bound.
        E.g. Random.randrange(0, 5) in [0, 1, 2, 3, 4] is always true
        '''
        return min + int(fmod(pow(clock() + pi, 2), 1.0) * (max - min))

    @staticmethod
    def shuffle(items):
        '''
        Method to shuffle a list of items
        '''
        i = len(items)
        while i > 1:
            i -= 1
            j = Random.randrange(0, i)
            items[j], items[i] = items[i], items[j]
        return items

if __name__ == '__main__':
    '''
    Not used as module, so run
    '''
    # seed my other random
    random.seed(datetime.now())
    Bot().run()