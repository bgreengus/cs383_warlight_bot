# -*- coding: utf-8 -*-
# Python 3.4*

'''
ASSIGNMENT INFO: Backprop via stochastic gradient descent for digit classification

Goal: Your goal is to customize this network to classify the digits.csv dataset correctly.

Data: In digits.csv, there are two columns 1 -- the pixels, and 2 -- the actual digit class, each row is an example.
A single line of the csv file represents a handwritten digit and its label. 
The digit is a 256-element vector obtained by flattening a 16x16 binary-valued image in row-major order;
the label is an integer representing the number in the picture. 
The data file contains 1593 instances with about 160 instances per digit.
If you want, you can try plotting these as a black and white image in 16x16.

You will need to make a new function to import the data, called importDigits(). 
This function should (obviously from the code) return 2 numpy arrays, of inputs and outputs. 


Runtime: In Python3, I should be able to import and run your file via:

import yourfilename
myNN = yourfilename.NN() # where your network size/structure is defined by optional __init__ specifications
trainX, trainY = yourfilename.importDigits(trainData.csv)
testX, testY = yourfilename.importDigits(testData.csv)
myNN.train(trainX, trainY)
grand_mean_error = myNN.test(testX, testY)

This means you should try making a second script with somethign like the above to execute your program, after you've done all your initial debugging. 

Note: Do NOT edit the calculating parts of the test function. I will split the data randomly between a test and a training set. Your goal is to get the best generalization. You can change this algorithm to have larger network depths (i.e., more layers) and change the size/structure of the network, and many other other network parameters, if you choose.

Grading: As before, auto-grading scripts will be used, so pay attention to detail! You will get some small part of your points determined by grand_mean_error (above). A valid low-performing program will get 90% credit. The top performing student's program will receive the full remaining 10%, and then the percentage of that performance will directly proportional to the allotment of the remaining 10% of points for all other students' valid programs.

Submission: Submit on moodle as before, on time, or not at all.

Questions: Email the course email, or come to office hours, or both!

PROGRAM INFO:
Note that np.dot() is the dot product, which is a cute and efficient way to represent the summation of the weight-activation products (e.g., neuron summation); for more information see:
https://en.wikipedia.org/wiki/Matrix_multiplication
https://en.wikipedia.org/wiki/Dot_product
hint: think about the basic formula for a neuron!

Variable Definitions
X 	Input dataset matrix where each row is a training example

y 	Output dataset matrix where each row is a training example

l0 	First Layer of the Network, specified by the input data

l1 	Second Layer of the Network, otherwise known as the hidden layer

l2 	Final Layer of the Network, which is our hypothesis, and should approximate the correct answer as we train.

syn0 	First layer of weights, Synapse 0, connecting l0 to l1.

syn1 	Second layer of weights, Synapse 1 connecting l1 to l2.

l2_error 	This is the amount that the neural network "missed".

l2_delta 	This is the error of the network scaled by the confidence. It's almost identical to the error except that very confident errors are muted.

l1_error 	Weighting l2_delta by the weights in syn1, we can calculate the error in the middle/hidden layer.

l1_delta 	This is the l1 error of the network scaled by the confidence. Again, it's almost identical to the l1_error except that confident errors are muted.

'''

import csv
import numpy as np
np.random.seed(1)

#You will need to make a new function to import the data, called importDigits(). 
#This function should (obviously from the code) return 2 numpy arrays, of inputs and outputs. 
def importDigits(csv_file):
	arr1 = []
	arr2 = []
	with open(csv_file, 'rb') as csvf:
		file_reader = csv.reader(csvf, dialect='excel')
		for row in file_reader:
			arr1.append(row[0])
			arr2.append(row[1])
	return np.array(arr1), np.array(arr2)
		


def sigmoid(x):
    return 1.0/(1.0 + np.exp(-x))


def sigmoid_prime(x):
    return sigmoid(x)*(1.0-sigmoid(x))


# hyperbolic tangent is often better than sigmoid, and you're welcome to try it, or others
def tanh(x):
    return 1.0 - np.tanh(x)**2


def tanh_prime(x):
    return 1.0 - x**2


class NN:
    def __init__(self, nI=3, nH=4, nO=1):
        # Randomly initialize our network weights with mean 0
        # This implicitly initializes nodes, which become explicit in runNN
        # first Input layer (nI) has 3 inputs, second Hidden (nH) has 4 neurons
        self.syn0 = 2 * np.random.random((nI, nH)) - 1
        # from the 4 Hidden to the 1 Output (nO)
        self.syn1 = 2 * np.random.random((nH, nO)) - 1

    def runNN(self, X):
        # Feed forward activity through layers 0, 1, and 2
        self.l0 = X
        self.l1 = sigmoid(np.dot(self.l0, self.syn0))
        self.l2 = sigmoid(np.dot(self.l1, self.syn1))
        return self.l2

    def backPropagate(self, y, N):
        # by how much did we miss the target value?
        # could use a variety of errors here, e.g., MSE, etc
        l2_error = y - self.l2

        # in what direction is the target value?
        # were we really sure? if so, don't change too much.
        l2_delta = l2_error * sigmoid_prime(self.l2)

        # how much did each l1 value contribute to the l2 error,
        # according to the weights?
        l1_error = l2_delta.dot(self.syn1.T)

        # in what direction is the target l1?
        # were we really sure? if so, don't change too much.
        l1_delta = l1_error * sigmoid_prime(self.l1)

        # update weights with learning rate = N
        self.syn1 += (self.l1.T.dot(l2_delta)) * N
        self.syn0 += (self.l0.T.dot(l1_delta)) * N

    def train(self, X, y, max_iterations=10000, N=.8):
        for round in range(max_iterations):
            self.runNN(X)
            self.backPropagate(y, N)

    def test(self, X, y):
        # Do NOT edit anything in this function except the print statments!
        final_prediction = self.runNN(X)
        print('Final classification outputs are: ')
        print(final_prediction)
        print('Final classification outputs was supposed to be: ')
        print(y)
        print('mean error is: ', np.mean((final_prediction - y)**2))
        return np.mean((final_prediction - y)**2)


def main():
    # a non-linear problem kind of like XOR, and could easily solve XOR
    X = np.array([[0, 0, 1],
                  [0, 1, 1],
                  [1, 0, 1],
                  [1, 1, 1]])

    y = np.array([[0],
                  [1],
            	   [1],
        		   [0]])

    myNN = NN(3, 4, 1)
    myNN.train(X, y)
    myNN.test(X, y)

if __name__ == "__main__":
    main()
