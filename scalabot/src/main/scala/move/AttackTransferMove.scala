/**
 * Copyright 2014 ramn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 *
 * Warlight AI Game Bot
 *
 * Last update: April 09, 2014
 *
 * @author ramn
 * @version 1.0
 * @source https://github.com/ramn/warlight-starterbot-scala
 */

package move

import main.Region


class AttackTransferMove(
  playerName: String,
  fromRegion: Region,
  toRegion: Region,
  armies: Int) extends Move {

    var illegalMove = ""

    def getString =
    if (illegalMove.isEmpty){
        s"$playerName attack/transfer ${fromRegion.id} ${toRegion.id} $armies"
    }else{
        s"$playerName illegal_move $illegalMove"
    }
}
