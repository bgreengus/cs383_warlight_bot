#---------------------------------------------------------------------#
# Warlight AI Challenge - Starter Bot                                 #
# ============                                                        #
#                                                                     #
# Last update: 20 Mar, 2014                                           #
#                                                                     #
# @author Jackie <jackie@starapple.nl>                                #
# @version 1.0                                                        #
# @license MIT License (http://opensource.org/licenses/MIT)           # 
#---------------------------------------------------------------------#

from math import fmod, pi
from sys import stderr, stdin, stdout
from time import clock
import random
from datetime import datetime
import numpy as np

class Bot(object):
    '''
    Main bot class
    '''
    def __init__(self):
        '''
        Initializes a map instance and an empty dict for settings
        '''
        self.settings = {}
        self.map = Map()

    def run(self):
        '''
        Main loop
        
        Keeps running while being fed data from stdin.
        Writes output to stdout, remember to flush!
        '''
        while not stdin.closed:
            try:
                rawline = stdin.readline()

                # End of file check
                if len(rawline) == 0:
                    break

                line = rawline.strip()

                # Empty lines can be ignored
                if len(line) == 0:
                    continue

                parts = line.split()

                command = parts[0]

                # All different commands besides the opponents' moves
                if command == 'settings':
                    self.update_settings(parts[1:])

                elif command == 'setup_map':
                    self.setup_map(parts[1:])

                elif command == 'update_map':
                    self.update_map(parts[1:])

                elif command == 'pick_starting_regions':
                    stdout.write(self.pick_starting_regions(parts[2:]) + '\n')
                    stdout.flush()

                elif command == 'opponent_moves':
                    stdout.flush()

                elif command == 'go':

                    sub_command = parts[1]

                    if sub_command == 'place_armies':

                        stdout.write(self.place_troops() + '\n')
                        stdout.flush()

                    elif sub_command == 'attack/transfer':

                        stdout.write(self.attack_transfer() + '\n')
                        stdout.flush()

                    else:
                        stderr.write('Unknown sub command: %s\n' % (sub_command))
                        stderr.flush()

                else:
                    stderr.write('Unknown command: %s\n' % (command))
                    stderr.flush()
            except EOFError:
                return
    
    def update_settings(self, options):
        '''
        Method to update game settings at the start of a new game.
        '''
        key, value = options
        self.settings[key] = value

    def setup_map(self, options):
        '''
        Method to set up essential map data given by the server.
        '''
        map_type = options[0]

        for i in range(1, len(options), 2):

            if map_type == 'super_regions':

                super_region = SuperRegion(options[i], int(options[i + 1]))
                self.map.super_regions.append(super_region)

            elif map_type == 'regions':

                super_region = self.map.get_super_region_by_id(options[i + 1])
                region = Region(options[i], super_region)
                
                self.map.regions.append(region)
                super_region.regions.append(region)

            elif map_type == 'neighbors':

                region = self.map.get_region_by_id(options[i])
                neighbours = [self.map.get_region_by_id(region_id) for region_id in options[i + 1].split(',')]

                for neighbour in neighbours:
                    region.neighbours.append(neighbour)
                    neighbour.neighbours.append(region)

        if map_type == 'neighbors':
            
            for region in self.map.regions:

                if region.is_on_super_region_border:
                    continue

                for neighbour in region.neighbours:

                    if neighbour.super_region.id != region.super_region.id:

                        region.is_on_super_region_border = True
                        neighbour.is_on_super_region_border = True

    def update_map(self, options):
        '''
        Method to update our map every round.
        '''
        # save old map
        old_owned_regions = self.map.get_owned_regions(self.settings['your_bot'])

        # update map
        for i in range(0, len(options), 3):
            
            region = self.map.get_region_by_id(options[i])
            region.owner = options[i + 1]
            region.troop_count = int(options[i + 2])

        # assign reward to (s,a) pairs
        for region in old_owned_regions:
            # todo fix this, bc will not work if region taken over
            region.assign_reward(self.settings['your_bot'])

            
    def pick_starting_regions(self, options):
        '''
        Method to select our initial starting regions.
        
        Currently selects six random regions.
        '''
        #shuffled_regions = Random.shuffle(Random.shuffle(options))
        #return ' '.join(shuffled_regions[:6])
        shuffled_regions = Random.shuffle(options)
        random.shuffle(options)
        SA_NA_AUS = ["1","2","3","4","5","6","7","8","9","10","11","12","13","39","40","41","42"]
        chosen_regions = []
        for region in shuffled_regions:
            if region in SA_NA_AUS:
                chosen_regions.append(region)
        return ' '.join(chosen_regions)

    def place_troops(self):
        '''
        Method to place our troops.
        
        Currently keeps placing a maximum of two troops on random regions.
        '''
        placements = []
        place_size = 2
        region_index = 0
        troops_remaining = int(self.settings['starting_armies'])
        
        owned_regions = self.map.get_owned_regions(self.settings['your_bot'])
        
        # states:   number enemies on border
        #           relative strength of enemies
        #           relative strength of me
        #           on super region border
        #           unowned regions in my super region
        #duplicated_regions = owned_regions * (3 + int(troops_remaining / 2))
        #shuffled_regions = Random.shuffle(duplicated_regions)

        # this is pretty good, but struggles at endgame
        p0 = [r for r in owned_regions if r.hasEnemyNeighbours() and r.is_on_super_region_border]
        p1 = [r for r in owned_regions if r.hasEnemyNeighbours()]
        p3 = [r for r in owned_regions if r.is_on_super_region_border]
        p2 = [r for r in owned_regions if r.hasNonMeNeighbours()]
        shuffled_regions = Random.shuffle(p0+p0+p0+p0+p1+p1+p1+p2+p2+p3+p3+owned_regions)
        while troops_remaining:

            region = shuffled_regions[region_index]
            
            if troops_remaining > place_size-1:
                placements.append([region.id, place_size])
                region.troop_count += place_size
                troops_remaining -= place_size
                
            else:
                 placements.append([region.id, 1])
                 region.troop_count += 1
                 troops_remaining -= 1

            region_index += 1
        


        return ', '.join(['%s place_armies %s %d' % (self.settings['your_bot'], placement[0],
            placement[1]) for placement in placements])

    def attack_transfer(self):
        '''
        Method to attack another region or transfer troops to allied regions.
        
        Currently checks whether a region has more than six troops placed to attack,
        or transfers if more than 1 unit is available.
        '''
        attack_transfers = []
        owned_regions = self.map.get_owned_regions(self.settings['your_bot'])
        usable_regions = [r for r in owned_regions if r.troop_count > 1]
        unusable_regions = [r for r in owned_regions if r.troop_count <= 1]
        for region in usable_regions:

            state, best_action = region.get_max_state_action_reward()

            if (best_action[0] > 0 and best_action[1] == 1):
                attack_transfers.append([region.id, best_action[0], region.troop_count-1])
            elif (best_action[0] > 0 and best_action[1] == 2):
                attack_transfers.append([region.id, best_action[0], region.troop_count/2])   
            region.last_action = best_action
            region.last_state = state
    
        for region in unusable_regions:
            region.last_action = None
            region.last_state = None

        if len(attack_transfers) == 0:
            return 'No moves'
        
        return ', '.join(['%s attack/transfer %s %s %s' % (self.settings['your_bot'], attack_transfer[0],
            attack_transfer[1], attack_transfer[2]) for attack_transfer in attack_transfers])


class Map(object):
    '''
    Map class
    '''
    def __init__(self):
        '''
        Initializes empty lists for regions and super regions.
        '''
        self.regions = []
        self.super_regions = []

    def get_region_by_id(self, region_id):
        '''
        Returns a region instance by id.
        '''
        return [region for region in self.regions if region.id == region_id][0]
    
    def get_super_region_by_id(self, super_region_id):
        '''
        Returns a super region instance by id.
        '''
        return [super_region for super_region in self.super_regions if super_region.id == super_region_id][0]

    def get_owned_regions(self, owner):
        '''
        Returns a list of region instances owned by `owner`.
        '''
        return [region for region in self.regions if region.owner == owner]

class SuperRegion(object):
    '''
    Super Region class
    '''
    def __init__(self, super_region_id, worth):
        '''
        Initializes with an id, the super region's worth and an empty lists for 
        regions located inside this super region
        '''
        self.id = super_region_id
        self.worth = worth
        self.regions = []

class Region(object):
    '''
    Region class
    '''
    def __init__(self, region_id, super_region):
        '''
        '''
        self.id = region_id
        self.owner = 'neutral'
        self.neighbours = []
        self.troop_count = 2
        self.super_region = super_region
        self.is_on_super_region_border = False
        self.action_list = None
        self.state_action_rewards = {}

    def get_state(self):
        """returns statelist string"""

        num_enemy_neighbours = len(self.getEnemyNeighbours())
        num_enemy_neighbours_in_SR = len(self.getEnemyNeighboursInSuper())
        num_neutral_neighbors = len(self.getNeutralNeighbours())
        num_neutral_neighbours_in_SR = len(self.getNeutralNeighboursInSuper())
        num_allied_neighbours = len(self.getAlliedNeighbours())
        has_troops_LE_allied_neighbours = all([self.troop_count <= r.troop_count for r in self.getAlliedNeighbours()])
        num_troops = int(self.troop_count / 6)
        has_more_troops_than_enemies = self.troop_count > sum([r.troop_count for r in self.getNonMeNeighbours()])

        # add state to list
        state = [num_enemy_neighbours,num_enemy_neighbours_in_SR,num_neutral_neighbors,num_neutral_neighbours_in_SR,num_allied_neighbours, has_troops_LE_allied_neighbours, num_troops, has_more_troops_than_enemies]
        statestr = str(state)
        if not statestr in self.state_action_rewards:
            self.state_action_rewards[statestr] = {}

        return state

    def get_max_state_action_reward(self):
        state = self.get_state()
        statestr = str(state)
        if not self.action_list:
            l1 = [(r.id, 1) for r in self.neighbours]
            l2 = [(r.id, 2) for r in self.neighbours]
            self.action_list = l1 + l2 + [(0,0)]

        if not statestr in self.state_action_rewards:
            self.state_action_rewards[statestr] = {}

        for a in self.action_list:
            if not a in self.state_action_rewards[statestr]:
                self.state_action_rewards[statestr][str(a)] = {'num_ran': 1.0, 'total_reward': 0.0}

        action = None
        r = -9999999999
        for a in self.action_list:
            data = self.state_action_rewards[statestr][str(a)]
            avg_r = (data['total_reward']/data['num_ran'])
            if avg_r > r:
                r = avg_r
                action = a
        # small chance of choosing random, maybe turn on only for exploratory mode.
        if np.random.rand() < .1:
            action = Random.shuffle(self.action_list)[0]

        return state, action

    def utility(self, newstate):
        reward = 0
        if newstate[0] < self.last_state[0]: # enemy n
            reward += 10
        if newstate[1] < self.last_state[1]: # enemy n sr
            reward += 20
        if newstate[2] < self.last_state[2]: # neutral n
            reward += 5
        if newstate[3] < self.last_state[3]: # neutral n sr
            reward += 10
        if newstate[4] < self.last_state[4]: # allied n
            reward += -5
        if newstate[5] == self.last_state[5]: # has LE surounding allies
            reward += 1
        if newstate[0] == self.last_state[0]: # enemy n
            reward += -3
        if newstate[0] > self.last_state[0]: # enemy n
            reward += -5
        if newstate[1] > self.last_state[1]: # enemy n sr
            reward +=  -10
        if newstate[4] > self.last_state[4]: # allied n
            reward += 1
        if newstate[5] != self.last_state[5]: # has LE surounding allies
            reward += -12
        if newstate[6] > self.last_state[6]:
            reward += -2
        return reward

    def assign_reward(self, prevowner):
        newstate = list(self.get_state())
        if self.last_action is None:
            return
        # get reward
        if self.owner != prevowner:
            reward = -30
        else:
            reward = self.utility(newstate)
        #update
        self.state_action_rewards[str(self.last_state)][str(self.last_action)]['num_ran'] += 1
        self.state_action_rewards[str(self.last_state)][str(self.last_action)]['total_reward'] += reward

    def getNumNeighbours(self):
        return len(self.neighbours)

    def getEnemyNeighbours(self):
        return [r for r in self.neighbours if r.owner != self.owner and r.owner != 'neutral']
    
    def getAlliedNeighbours(self):
        return [r for r in self.neighbours if r.owner == self.owner]

    def getNonMeNeighbours(self):
        return [r for r in self.neighbours if r.owner != self.owner]

    def getEnemyNeighboursInSuper(self):
        return [r for r in self.neighbours if r.owner != self.owner and r.super_region.id == self.super_region.id and r.owner != 'neutral']

    def getNonMeNeighboursInSuper(self):
        return [r for r in self.neighbours if r.owner != self.owner and r.super_region.id == self.super_region.id and r.owner != 'neutral']

    def getNeutralNeighbours(self):
        return [r for r in self.neighbours if r.owner is 'neutral']

    def getNeutralNeighboursInSuper(self):
        return [r for r in self.neighbours if r.owner is 'neutral' and r.super_region.id == self.super_region.id]

    def hasNonMeNeighbours(self):
        return len(self.getNonMeNeighbours()) > 0

    def hasEnemyNeighbours(self):
        return len(self.getEnemyNeighbours()) > 0

    def getStrongestEnemyNeighbour(self):
        mx = 0
        strongest = None
        for r in self.neighbours:
            if mx < r.troop_count:
                mx = r.troop_count
                strongest = r
        return r

class Random(object):
    '''
    Random class
    '''
    @staticmethod
    def randrange(min, max):
        '''
        A pseudo random number generator to replace random.randrange
        
        Works with an inclusive left bound and exclusive right bound.
        E.g. Random.randrange(0, 5) in [0, 1, 2, 3, 4] is always true
        '''
        return min + int(fmod(pow(clock() + pi, 2), 1.0) * (max - min))

    @staticmethod
    def shuffle(items):
        '''
        Method to shuffle a list of items
        '''
        i = len(items)
        while i > 1:
            i -= 1
            j = Random.randrange(0, i)
            items[j], items[i] = items[i], items[j]
        return items


if __name__ == '__main__':
    '''
    Not used as module, so run
    '''
    # seed my other random
    random.seed(datetime.now())
    Bot().run()