/**
 * Copyright 2014 ramn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 *
 * Warlight AI Game Bot
 *
 * Last update: April 09, 2014
 *
 * @author ramn
 * @version 1.0
 * @source https://github.com/ramn/warlight-starterbot-scala
 */

package bot

import collection.immutable.Seq

import main.Region
import main.SuperRegion
import move.AttackTransferMove
import move.PlaceArmiesMove


object BotStarter {
  def main(args: Array[String]) {
    val parser = new BotParser(new BotStarter)
    parser.run()
  }
}


class BotStarter extends Bot {

    /* needs to be collection of 6 [Region] */
    /* priotitze NA SA AUS, TODO next prioritize border of SA NA */
    override def getPreferredStartingRegions(state: BotState, timeOut: Long): Seq[Region] = {
        // prioritize NA SA Aus
        val na_sa_aust = List(1,2,3,4,5,6,7,8,9,10,11,12,13,39,40,41,42)
        // remove not great regions
        val good = state.pickableStartingRegions.filter(x => na_sa_aust.exists(y => y == x.id))
        if (good.length < 6)
            util.Random.shuffle(state.pickableStartingRegions.toList ::: good.toList).take(6)
        else
            util.Random.shuffle(good).take(6)
     
           //util.Random.shuffle(state.pickableStartingRegions).take(6)
    }

    /* currently place all on one territory, TODO prioritize border placement */
    override def getPlaceArmiesMoves(state: BotState, timeOut: Long): Seq[PlaceArmiesMove] = {
        val myName = state.playerName
        //val armiesToPlacePerRegion = 2
        val myVisibleRegions = state.visibleMap.regions.filter(_.ownedByPlayer(myName))
        /*val deploymentSizes = {
            val completeDeploysCount = (state.startingArmies / armiesToPlacePerRegion).toInt
            val rest = state.startingArmies % armiesToPlacePerRegion
            val deploys = Seq.fill(completeDeploysCount)(armiesToPlacePerRegion)
            if (rest > 0)
                deploys :+ rest
            else
                deploys
        }*/
        val deploymentSizes = Seq(state.startingArmies)
        val regionsInRandomOrder = util.Random.shuffle(myVisibleRegions)
        def makeMove(spec: (Region, Int)) = {
            val (region, deploySize) = spec
            new PlaceArmiesMove(myName, region, deploySize)
        }
        val moves = regionsInRandomOrder zip deploymentSizes map makeMove
        moves.toIndexedSeq
    }
  
    override def getAttackTransferMoves(
    state: BotState,
    timeOut: Long
  ): Seq[AttackTransferMove] = {
    val myName = state.playerName
    val armiesToMove = 5
    val myVisibleRegions = state.visibleMap.regions.filter(_.ownedByPlayer(myName))

    def pickMove(fromRegion: Region): Option[AttackTransferMove] = {
      val possibleToRegions = util.Random.shuffle(fromRegion.neighbors)
      // TODO: use find instead of flatMap, so we only create one Move instance
      possibleToRegions.flatMap { toRegion =>
        val eligibleForAttack =
          (toRegion.playerName != myName && fromRegion.armies > 6)
        val eligibleForTransfer =
          (toRegion.playerName == myName && fromRegion.armies > 1)
        if (eligibleForAttack || eligibleForTransfer)
          Some(new AttackTransferMove(myName, fromRegion, toRegion, armiesToMove))
        else
          None
      }.headOption
    }
    val moves = myVisibleRegions flatMap pickMove
    moves.toIndexedSeq
  }
}
