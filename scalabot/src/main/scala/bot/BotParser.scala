/**
 * Copyright 2014 ramn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 *
 * Warlight AI Game Bot
 *
 * Last update: April 09, 2014
 *
 * @author ramn
 * @version 1.0
 * @source https://github.com/ramn/warlight-starterbot-scala
 */

package bot

import java.util.Scanner
import collection.immutable.IndexedSeq

import main.Region
import move.PlaceArmiesMove
import move.AttackTransferMove


class BotParser(bot: Bot) {

  val scan = new Scanner(System.in)

  var currentState: BotState = new BotState

  def run() {
    while (scan.hasNextLine) {
      val line = scan.nextLine.trim
      if (!line.isEmpty) {
        handleLine(line)
      }
    }
  }

  def handleLine(line: String): Unit = {
    val parts = IndexedSeq(line.split(" "):_*)
    val command = parts(0)
    command match {
      case "pick_starting_regions" =>
        currentState.setPickableStartingRegions(parts)
        val timeout = parts(1).toLong
        val output = bot
          .getPreferredStartingRegions(currentState, timeout)
          .map(_.id)
          .mkString(" ")
        println(output)
      case "go" if parts.length == 3 =>
        val timeout = parts(2).toLong
        val action = parts(1)
        val moves = action match {
          case "place_armies" =>
            bot.getPlaceArmiesMoves(currentState, timeout)
          case "attack/transfer" =>
            bot.getAttackTransferMoves(currentState, timeout)
        }
        val output = moves.map(_.getString).mkString(",")
        if (!output.isEmpty) {
          println(output)
        } else {
          println("No moves")
        }
      case "settings" if parts.length == 3 =>
        currentState.updateSettings(parts(1), parts(2))
      case "setup_map" =>
        currentState.setupMap(parts)
      case "update_map" =>
        currentState.updateMap(parts)
      case "opponent_moves" =>
        currentState.readOpponentMoves(parts)
      case _ =>
        System.err.println(s"Unable to parse line '$line'")
    }
  }
}

