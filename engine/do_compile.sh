#!/bin/bash
rm -r classes
ls ./main/*.java > sources.txt
ls ./move/*.java >> sources.txt
ls ./io/*.java >> sources.txt
ls ./bot/*.java >> sources.txt
mkdir classes
javac -d classes @sources.txt
rm sources.txt
