#---------------------------------------------------------------------#
# Warlight AI Challenge - Starter Bot                                 #
# ============                                                        #
#                                                                     #
# Last update: 20 Mar, 2014                                           #
#                                                                     #
# @author Jackie <jackie@starapple.nl>                                #
# @version 1.0                                                        #
# @license MIT License (http://opensource.org/licenses/MIT)           # 
#---------------------------------------------------------------------#

from math import fmod, pi
from sys import stderr, stdin, stdout
from time import clock
import random
from datetime import datetime

class Bot(object):
    '''
    Main bot class
    '''
    def __init__(self):
        '''
        Initializes a map instance and an empty dict for settings
        '''
        self.settings = {}
        self.map = Map()

    def run(self):
        '''
        Main loop
        
        Keeps running while being fed data from stdin.
        Writes output to stdout, remember to flush!
        '''
        while not stdin.closed:
            try:
                rawline = stdin.readline()

                # End of file check
                if len(rawline) == 0:
                    break

                line = rawline.strip()

                # Empty lines can be ignored
                if len(line) == 0:
                    continue

                parts = line.split()

                command = parts[0]

                # All different commands besides the opponents' moves
                if command == 'settings':
                    self.update_settings(parts[1:])

                elif command == 'setup_map':
                    self.setup_map(parts[1:])

                elif command == 'update_map':
                    self.update_map(parts[1:])

                elif command == 'pick_starting_regions':
                    stdout.write(self.pick_starting_regions(parts[2:]) + '\n')
                    stdout.flush()

                elif command == 'opponent_moves':
                    stdout.flush()

                elif command == 'go':

                    sub_command = parts[1]

                    if sub_command == 'place_armies':

                        stdout.write(self.place_troops() + '\n')
                        stdout.flush()

                    elif sub_command == 'attack/transfer':

                        stdout.write(self.attack_transfer() + '\n')
                        stdout.flush()

                    else:
                        stderr.write('Unknown sub command: %s\n' % (sub_command))
                        stderr.flush()

                else:
                    stderr.write('Unknown command: %s\n' % (command))
                    stderr.flush()
            except EOFError:
                return
    
    def update_settings(self, options):
        '''
        Method to update game settings at the start of a new game.
        '''
        key, value = options
        self.settings[key] = value

    def setup_map(self, options):
        '''
        Method to set up essential map data given by the server.
        '''
        map_type = options[0]

        for i in range(1, len(options), 2):

            if map_type == 'super_regions':

                super_region = SuperRegion(options[i], int(options[i + 1]))
                self.map.super_regions.append(super_region)

            elif map_type == 'regions':

                super_region = self.map.get_super_region_by_id(options[i + 1])
                region = Region(options[i], super_region)
                
                self.map.regions.append(region)
                super_region.regions.append(region)

            elif map_type == 'neighbors':

                region = self.map.get_region_by_id(options[i])
                neighbours = [self.map.get_region_by_id(region_id) for region_id in options[i + 1].split(',')]

                for neighbour in neighbours:
                    region.neighbours.append(neighbour)
                    neighbour.neighbours.append(region)

        if map_type == 'neighbors':
            
            for region in self.map.regions:

                if region.is_on_super_region_border:
                    continue

                for neighbour in region.neighbours:

                    if neighbour.super_region.id != region.super_region.id:

                        region.is_on_super_region_border = True
                        neighbour.is_on_super_region_border = True

    def update_map(self, options):
        '''
        Method to update our map every round.
        '''
        # save old map
        old_owned_regions = self.map.get_owned_regions(self.settings['your_bot'])

        # update map
        for i in range(0, len(options), 3):
            
            region = self.map.get_region_by_id(options[i])
            region.owner = options[i + 1]
            region.troop_count = int(options[i + 2])

        # assign reward to (s,a) pairs
        for region in old_owned_regions:
            # todo fix this, bc will not work if region taken over
            region.assign_reward(self.settings['your_bot'])

            
    def pick_starting_regions(self, options):
        '''
        Method to select our initial starting regions.
        
        Currently selects six random regions.
        '''
        #shuffled_regions = Random.shuffle(Random.shuffle(options))
        #return ' '.join(shuffled_regions[:6])
        shuffled_regions = Random.shuffle(options)
        random.shuffle(options)
        SA_NA_AUS = ["1","2","3","4","5","6","7","8","9","10","11","12","13","39","40","41","42"]
        chosen_regions = []
        for region in shuffled_regions:
            if region in SA_NA_AUS:
                chosen_regions.append(region)
        return ' '.join(chosen_regions)

    def place_troops(self):
        '''
        Method to place our troops.
        
        Currently keeps placing a maximum of two troops on random regions.
        '''
        placements = []
        place_size = 2
        region_index = 0
        troops_remaining = int(self.settings['starting_armies'])
        
        owned_regions = self.map.get_owned_regions(self.settings['your_bot'])
        
        # states:   number enemies on border
        #           relative strength of enemies
        #           relative strength of me
        #           on super region border
        #           unowned regions in my super region
        #duplicated_regions = owned_regions * (3 + int(troops_remaining / 2))
        #shuffled_regions = Random.shuffle(duplicated_regions)

        # this is pretty good, but struggles at endgame
        p0 = [r for r in owned_regions if r.hasEnemyNeighbours() and r.is_on_super_region_border]
        p1 = [r for r in owned_regions if r.hasEnemyNeighbours()]
        p3 = [r for r in owned_regions if r.is_on_super_region_border]
        p2 = [r for r in owned_regions if r.hasNonMeNeighbours()]
        shuffled_regions = Random.shuffle(p0+p0+p0+p0+p1+p1+p1+p2+p2+p3+p3+owned_regions)
        while troops_remaining:

            region = shuffled_regions[region_index]
            
            if troops_remaining > place_size-1:
                placements.append([region.id, place_size])
                region.troop_count += place_size
                troops_remaining -= place_size
                
            else:
                 placements.append([region.id, 1])
                 region.troop_count += 1
                 troops_remaining -= 1

            region_index += 1
        


        return ', '.join(['%s place_armies %s %d' % (self.settings['your_bot'], placement[0],
            placement[1]) for placement in placements])

    def attack_transfer(self):
        '''
        Method to attack another region or transfer troops to allied regions.
        
        Currently checks whether a region has more than six troops placed to attack,
        or transfers if more than 1 unit is available.
        '''
        attack_transfers = []
        owned_regions = self.map.get_owned_regions(self.settings['your_bot'])
        usable_regions = [r for r in owned_regions if r.troop_count > 1]
        unusable_regions = [r for r in owned_regions if r.troop_count <= 1]
        for region in usable_regions:

            state, best_action = get_max_state_action_reward(region_statestr)

            if (best_action[0] > 0 and best_action[1] == 1):
                attack_transfers.append([region.id, best_action[0], region.troop_count-1])
            elif (best_action[0] > 0 and best_action[1] == 2):
                attack_transfers.append([region.id, best_action[0], region.troop_count/2])   
            region.last_action = best_action
            region.last_state = state
    
        for region in unusable_regions:
            region.last_action = None
            region.last_state = None

        if len(attack_transfers) == 0:
            return 'No moves'
        
        return ', '.join(['%s attack/transfer %s %s %s' % (self.settings['your_bot'], attack_transfer[0],
            attack_transfer[1], attack_transfer[2]) for attack_transfer in attack_transfers])


class Map(object):
    '''
    Map class
    '''
    def __init__(self):
        '''
        Initializes empty lists for regions and super regions.
        '''
        self.regions = []
        self.super_regions = []

    def get_region_by_id(self, region_id):
        '''
        Returns a region instance by id.
        '''
        return [region for region in self.regions if region.id == region_id][0]
    
    def get_super_region_by_id(self, super_region_id):
        '''
        Returns a super region instance by id.
        '''
        return [super_region for super_region in self.super_regions if super_region.id == super_region_id][0]

    def get_owned_regions(self, owner):
        '''
        Returns a list of region instances owned by `owner`.
        '''
        return [region for region in self.regions if region.owner == owner]

class SuperRegion(object):
    '''
    Super Region class
    '''
    def __init__(self, super_region_id, worth):
        '''
        Initializes with an id, the super region's worth and an empty lists for 
        regions located inside this super region
        '''
        self.id = super_region_id
        self.worth = worth
        self.regions = []

class Region(object):
    '''
    Region class
    '''
    def __init__(self, region_id, super_region):
        '''
        '''
        self.id = region_id
        self.owner = 'neutral'
        self.neighbours = []
        self.troop_count = 2
        self.super_region = super_region
        self.is_on_super_region_border = False
        self.action_list = None
        self.state_action_rewards = {}

    def get_state(self):
        """returns statelist string"""

        num_enemy_neighbours = len(self.getEnemyNeighbours())
        num_enemy_neighbours_in_SR = len(self.getEnemyNeighboursInSuper())
        num_neutral_neighbors = len(self.getNeutralNeighbours())
        num_neutral_neighbours_in_SR = len(self.getNeutralNeighboursInSuper())
        num_allied_neighbours = len(self.getAlliedNeighbours())
        has_troop_LE_allied_neighbours = all([self.troop_count <= r.troop_count for r in self.getAlliedNeighbours()])
        num_troops = int(self.troop_count / 6)
        has_more_troops_than_enemies = self.troop_count > sum([r.troop_count for r in self.getNonMeNeighbours])

        # add state to list
        statestr = str([num_enemy_neighbours,num_enemy_neighbours_in_SR,num_neutral_neighbors,num_neutral_neighbours_in_SR,
                        num_allied_neighbours,has_troops_LE_allied_neighbours, num_troops, has_more_troops_than_enemies])

        if not statestr in self.state_action_rewards:
            self.state_action_rewards[statestr] = {}

        return str([num_enemy_neighbours,num_enemy_neighbours_in_SR,num_neutral_neighbors,num_neutral_neighbours_in_SR,
                num_allied_neighbours,has_troops_LE_allied_neighbours, num_troops])

    def get_max_state_action_reward(self, statestr):
        statestr = self.get_state()
        if not self.action_list:
            l1 = [str((r.id, 1)) for r in self.neighbours]
            l2 = [str((r.id, 2)) for r in self.neighbours]
            self.action_list = l1 + l2 + [str((0,0))]
        for a in self.action_list:
            if not a in self.state_action_rewards[statestr]:
                self.state_action_rewards[statstr][a] = {'num_ran': 1.0, 'total_reward': 0.0}

        action = None
        r = -9999999999
        for a in self.action_list:
            data = self.state_action_rewards[statestr][a]
            avg_r = (data['total_reward']/data['num_ran'])
            if avg_r > r:
                r = avg_r
                action = a

        return list(statestr), tuple(action)

    def utility(self, newstate):
        reward = 0
        if newstate[0] < self.last_state[0]: # enemy n
            reward += 10
        if newstate[1] < self.last_state[1]: # enemy n sr
            reward += 20
        if newstate[2] < self.last_state[2]: # neutral n
            reward += 5
        if newstate[3] < self.last_state[3]: # neutral n sr
            reward += 10
        if newstate[4] < self.last_state[4]: # allied n
            reward += -5
        if newstate[5] == self.last_state[5]: # has LE surounding allies
            reward += 1
        if newstate[0] > self.last_state[0]: # enemy n
            reward += -5
        if newstate[1] > self.last_state[1]: # enemy n sr
            reward +=  -10
        if newstate[4] > self.last_state[4]: # allied n
            reward += 1
        if newstate[5] != self.last_state[5]: # has LE surounding allies
            reward += -1
        if newstate[6] > self.last_state[6]:
            reward += -2
        return reward

    def assign_reward(self, newstatestr, prevowner):
        newstate = list(self.get_state())
        if self.last_action is None:
            return
        # get reward
        if self.owner != prevowner:
            reward = -30
        else:
            reward += utility(newstate)
        #update
        self.state_action_rewards[str(self.last_state)][str(self.last_action)]['num_ran'] += 1
        self.state_action_rewards[str(self.last_state)][str(self.last_action)]['total_reward'] += reward

    def getNumNeighbours(self):
        return len(self.neighbours)

    def getEnemyNeighbours(self):
        return [r for r in self.neighbours if r.owner != self.owner and r.owner != 'neutral']
    
    def getAlliedNeighbours(self):
        return [r for r in self.neighbours if r.owner == self.owner]

    def getNonMeNeighbours(self):
        return [r for r in self.neighbours if r.owner != self.owner]

    def getEnemyNeighboursInSuper(self):
        return [r for r in self.neighbours if r.owner != self.owner and r.super_region.id == self.super_region.id and r.owner != 'neutral']

    def getNonMeNeighboursInSuper(self):
        return [r for r in self.neighbours if r.owner != self.owner and r.super_region.id == self.super_region.id and r.owner != 'neutral']

    def getNeutralNeighbours(self):
        return [r for r in self.neighbours if r.owner is 'neutral']

    def getNeutralNeighboursInSuper(self):
        return [r for r in self.neighbours if r.owner is 'neutral' and r.super_region.id == self.super_region.id]

    def hasNonMeNeighbours(self):
        return len(self.getNonMeNeighbours()) > 0

    def hasEnemyNeighbours(self):
        return len(self.getEnemyNeighbours()) > 0

    def getStrongestEnemyNeighbour(self):
        mx = 0
        strongest = None
        for r in self.neighbours:
            if mx < r.troop_count:
                mx = r.troop_count
                strongest = r
        return r

class Random(object):
    '''
    Random class
    '''
    @staticmethod
    def randrange(min, max):
        '''
        A pseudo random number generator to replace random.randrange
        
        Works with an inclusive left bound and exclusive right bound.
        E.g. Random.randrange(0, 5) in [0, 1, 2, 3, 4] is always true
        '''
        return min + int(fmod(pow(clock() + pi, 2), 1.0) * (max - min))

    @staticmethod
    def shuffle(items):
        '''
        Method to shuffle a list of items
        '''
        i = len(items)
        while i > 1:
            i -= 1
            j = Random.randrange(0, i)
            items[j], items[i] = items[i], items[j]
        return items

"""Markov Decision Processes (Chapter 17)

First we define an MDP, and the special case of a GridMDP, in which
states are laid out in a 2-dimensional grid.  We also represent a policy
as a dictionary of {state:action} pairs, and a Utility function as a
dictionary of {state:number} pairs.  We then define the value_iteration
and policy_iteration algorithms."""

from utils import *

class MDP:
    """A Markov Decision Process, defined by an initial state, transition model,
    and reward function. We also keep track of a gamma value, for use by
    algorithms. The transition model is represented somewhat differently from
    the text.  Instead of T(s, a, s') being  probability number for each
    state/action/state triplet, we instead have T(s, a) return a list of (p, s')
    pairs.  We also keep track of the possible states, terminal states, and
    actions for each state. [page 615]"""

    def __init__(self, init, actlist, terminals, gamma=.9):
        update(self, init=init, actlist=actlist, terminals=terminals,
               gamma=gamma, states=set(), reward={})

    def R(self, state):
        "Return a numeric reward for this state."
        return self.reward[state]

    def T(state, action):
        """Transition model.  From a state and an action, return a list
        of (result-state, probability) pairs."""
        abstract

    def actions(self, state):
        """Set of actions that can be performed in this state.  By default, a
        fixed list of actions, except for terminal states. Override this
        method if you need to specialize by state."""
        if state in self.terminals:
            return [None]
        else:
            return self.actlist

class GridMDP(MDP):
    """A two-dimensional grid MDP, as in [Figure 17.1].  All you have to do is
    specify the grid as a list of lists of rewards; use None for an obstacle
    (unreachable state).  Also, you should specify the terminal states.
    An action is an (x, y) unit vector; e.g. (1, 0) means move east."""
    def __init__(self, grid, terminals, init=(0, 0), gamma=.9):
        grid.reverse() ## because we want row 0 on bottom, not on top
        MDP.__init__(self, init, actlist=orientations,
                     terminals=terminals, gamma=gamma)
        update(self, grid=grid, rows=len(grid), cols=len(grid[0]))
        for x in range(self.cols):
            for y in range(self.rows):
                self.reward[x, y] = grid[y][x]
                if grid[y][x] is not None:
                    self.states.add((x, y))

    def T(self, state, action):
        if action == None:
            return [(0.0, state)]
        else:
            return [(0.8, self.go(state, action)),
                    (0.1, self.go(state, turn_right(action))),
                    (0.1, self.go(state, turn_left(action)))]

    def go(self, state, direction):
        "Return the state that results from going in this direction."
        state1 = vector_add(state, direction)
        return if_(state1 in self.states, state1, state)

    def to_grid(self, mapping):
        """Convert a mapping from (x, y) to v into a [[..., v, ...]] grid."""
        return list(reversed([[mapping.get((x,y), None)
                               for x in range(self.cols)]
                              for y in range(self.rows)]))

    def to_arrows(self, policy):
        chars = {(1, 0):'>', (0, 1):'^', (-1, 0):'<', (0, -1):'v', None: '.'}
        return self.to_grid(dict([(s, chars[a]) for (s, a) in policy.items()]))

"""
Fig[17,1] = GridMDP([[-0.04, -0.04, -0.04, +1],
                     [-0.04, None,  -0.04, -1],
                     [-0.04, -0.04, -0.04, -0.04]],
                    terminals=[(3, 2), (3, 1)])
"""

def value_iteration(mdp, epsilon=0.001):
    "Solving an MDP by value iteration. [Fig. 17.4]"
    U1 = dict([(s, 0) for s in mdp.states])
    R, T, gamma = mdp.R, mdp.T, mdp.gamma
    while True:
        U = U1.copy()
        delta = 0
        for s in mdp.states:
            U1[s] = R(s) + gamma * max([sum([p * U[s1] for (p, s1) in T(s, a)])
                                        for a in mdp.actions(s)])
            delta = max(delta, abs(U1[s] - U[s]))
        if delta < epsilon * (1 - gamma) / gamma:
             return U

def best_policy(mdp, U):
    """Given an MDP and a utility function U, determine the best policy,
    as a mapping from state to action. (Equation 17.4)"""
    pi = {}
    for s in mdp.states:
        pi[s] = argmax(mdp.actions(s), lambda a:expected_utility(a, s, U, mdp))
    return pi

def expected_utility(a, s, U, mdp):
    "The expected utility of doing a in state s, according to the MDP and U."
    return sum([p * U[s1] for (p, s1) in mdp.T(s, a)])


def policy_iteration(mdp):
    "Solve an MDP by policy iteration [Fig. 17.7]"
    U = dict([(s, 0) for s in mdp.states])
    pi = dict([(s, random.choice(mdp.actions(s))) for s in mdp.states])
    while True:
        U = policy_evaluation(pi, U, mdp)
        unchanged = True
        for s in mdp.states:
            a = argmax(mdp.actions(s), lambda a: expected_utility(a,s,U,mdp))
            if a != pi[s]:
                pi[s] = a
                unchanged = False
        if unchanged:
            return pi

def policy_evaluation(pi, U, mdp, k=20):
    """Return an updated utility mapping U from each state in the MDP to its
    utility, using an approximation (modified policy iteration)."""
    R, T, gamma = mdp.R, mdp.T, mdp.gamma
    for i in range(k):
        for s in mdp.states:
            U[s] = R(s) + gamma * sum([p * U[s] for (p, s1) in T(s, pi[s])])
    return U



if __name__ == '__main__':
    '''
    Not used as module, so run
    '''
    # seed my other random
    random.seed(datetime.now())
    Bot().run()