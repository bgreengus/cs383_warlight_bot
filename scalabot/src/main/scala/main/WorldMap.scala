/**
 * Copyright 2014 ramn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 *
 * Warlight AI Game Bot
 *
 * Last update: April 09, 2014
 *
 * @author ramn
 * @version 1.0
 * @source https://github.com/ramn/warlight-starterbot-scala
 */

package main


class WorldMap {
  private var myRegions = Set.empty[Region]
  private var mySuperRegions = Set.empty[SuperRegion]

  def add(region: Region): Unit = {
    myRegions += region
  }

  def add(region: SuperRegion): Unit = {
    mySuperRegions += region
  }

  def getRegion(id: Int): Option[Region] = {
    regions find (_.id == id)
  }

  def getSuperRegion(id: Int): Option[SuperRegion] = {
    superRegions find (_.id == id)
  }

  def regions = myRegions

  def superRegions = mySuperRegions

  def removeRegion(region: Region): Unit = {
    myRegions = myRegions - region
  }

  def getMapString: String = {
    val regionsAsStrings = regions.map { r =>
      Seq(r.id, r.playerName, r.armies).mkString(";")
    }
    regionsAsStrings.mkString(" ")
  }

  def copy: WorldMap = {
    val newMap = new WorldMap

    for (sr <- superRegions) {
      newMap.add(new SuperRegion(sr.id, sr.reward))
    }
    for (r <- regions) {
      val superRegionOpt = newMap.getSuperRegion(r.superRegion.id)
      superRegionOpt foreach { superRegion =>
        val newRegion = new Region(r.id, superRegion, r.playerName, r.armies)
        newMap.add(newRegion)
      }
    }
    for (r <- regions) {
      val newRegionOpt = newMap.getRegion(r.id)
      for {
        newRegion <- newRegionOpt
        neighbor <- r.neighbors
        neighborRegion <- newMap.getRegion(neighbor.id)
      } newRegion.addNeighbor(neighborRegion)
    }
    newMap
  }
}
