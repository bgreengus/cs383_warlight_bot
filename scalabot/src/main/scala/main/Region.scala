/**
 * Copyright 2014 ramn
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 *
 * Warlight AI Game Bot
 *
 * Last update: April 09, 2014
 *
 * @author ramn
 * @version 1.0
 * @source https://github.com/ramn/warlight-starterbot-scala
 */

package main


class Region(
  val id: Int,
  val superRegion: SuperRegion,
  playerNameInit: String,
  armiesInit: Int
) {

  def this(id: Int, superRegion: SuperRegion) = {
    this(id, superRegion, "", 0)
  }

  private var myNeighbors = Set.empty[Region]
  var playerName: String = ""
  var armies: Int = 0

  def addNeighbor(region: Region): Unit = {
    myNeighbors += region
  }

  def neighbors = myNeighbors

  def ownedByPlayer(playerName: String): Boolean =
    playerName == this.playerName
}
